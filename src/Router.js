import React, { Component } from 'react'
import { Router, Scene, Actions } from 'react-native-router-flux'
import LoginForm from './components/LoginForm'
import EmployeeList from './components/EmployeeList'
import NewEmployee from './components/NewEmployee'

class RouterComponent extends Component {
  render() {
    return (
      <Router sceneStyle={{ paddingTop: 65 }}>
        <Scene key="auth">
          <Scene key="login" component={LoginForm} title="Login" />
        </Scene>
        <Scene key="main">
          <Scene
            key="employeeList"
            component={EmployeeList}
            title="Employees"
            rightTitle="Add"
            onRight={() => Actions.newEmployee()}
          />
          <Scene key="newEmployee" component={NewEmployee} title="New Employee" />
        </Scene>
      </Router>
    )
  }
}

export default RouterComponent
