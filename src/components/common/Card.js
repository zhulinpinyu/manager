import React, { Component } from 'react';
import { View } from 'react-native';

class Card extends Component {
  render() {
    return (
      <View style={styles.cardStyle}>
        { this.props.children }
      </View>
    );
  }
}

const styles = {
  cardStyle: {
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10
  }
};

export { Card };
