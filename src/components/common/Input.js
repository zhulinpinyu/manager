import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';

class Input extends Component {
  render() {
    const { label, placeholder, value, onChangeText, secureTextEntry } = this.props;
    const { containerStyle, labelStyle, inputStyle } = styles;
    return (
      <View style={containerStyle}>
        <Text style={labelStyle}>
          {label}
        </Text>
        <TextInput
          secureTextEntry={secureTextEntry}
          autoCorrect={false}
          autoCapitalize="none"
          placeholder={placeholder}
          value={value}
          onChangeText={onChangeText}
          style={inputStyle}
        />
      </View>
    );
  }
}

const styles = {
  inputStyle: {
    color: '#000',
    paddingLeft: 5,
    paddingRight: 5,
    fontSize: 18,
    lineHeight: 23,
    flex: 3
  },
  labelStyle: {
    fontSize: 18,
    paddingLeft: 20,
    fontWeight: '600',
    flex: 1
  },
  containerStyle: {
    height: 40,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  }
};

export { Input };
