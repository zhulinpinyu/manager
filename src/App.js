import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import reduxThunk from 'redux-thunk'
import firebase from 'firebase'
import reducers from './reducers'
import Router from './Router'

export default class App extends Component {

  componentWillMount() {
    const config = {
      apiKey: 'AIzaSyANEcsNl4PofyA_fTXtPa5dAkXLkFkQbQk',
      authDomain: 'manager-73738.firebaseapp.com',
      databaseURL: 'https://manager-73738.firebaseio.com',
      storageBucket: 'manager-73738.appspot.com',
      messagingSenderId: '1062129536668'
    }
    firebase.initializeApp(config)
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(reduxThunk))
    return (
      <Provider store={store}>
        <Router />
      </Provider>
    )
  }
}
