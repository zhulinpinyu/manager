import { EMPLOYEE_UPDATE } from '../actions/types'

const INIT_STATE = {
  shift: 'Monday'
}

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case EMPLOYEE_UPDATE:
      return { ...state, [action.payload.prop]: action.payload.value }
    default:
      return state
  }
}
